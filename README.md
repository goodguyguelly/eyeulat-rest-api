# EyeULAT - Rest API


## Project Description

> *\*INSERT PROJECT DESC\**

### Steps to setup EyeULAT - RestAPI

* Install [NodeJS](https://nodejs.org)

* Install [SourceTree](https://www.sourcetreeapp.com/) or any Git Client

* Clone this Repository

* Execute this commands after cloning

```
cd /path/to/eyeulat-rest-api
npm install
```