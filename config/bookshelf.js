import { knex as config } from './database';
import Knex from 'knex';
import Bookshelf from 'bookshelf';
import { pluggable } from 'bookshelf-modelbase';

const knex = Knex(config.development);
const bookshelf = Bookshelf(knex);

bookshelf.plugin('registry');
bookshelf.plugin('visibility');
bookshelf.plugin(pluggable);

export default bookshelf;