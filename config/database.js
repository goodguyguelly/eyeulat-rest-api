
const knex = {
    development: {
      client: process.env.DEV_DB_DRIVER,
      connection: {
        host     : process.env.DEV_DB_HOST,
        user     : process.env.DEV_DB_USERNAME,
        password : process.env.DEV_DB_PASSWORD,
        database : process.env.DEV_DB_NAME,
        charset  : 'utf8'
      }
    }
};

export { knex } 