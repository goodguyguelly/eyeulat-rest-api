import Bookshelf from './bookshelf';

let modelbase = Bookshelf;

modelbase.Model = modelbase.Model.extend({
    hasTimestamps: true,
});

export default modelbase;