
exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('r_roles', function (table) {
            table.increments();
            table.string('code');
            table.string('name');
            table.timestamps();
        })
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('r_roles')
    ]);
};
