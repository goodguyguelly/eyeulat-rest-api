import App from '../config/model';

class User extends App.Model {
    get tableName() { return 'r_users'; }

    get hidden() {
        return [
            'password'
        ];
    }
}

export default App.model('User', User);