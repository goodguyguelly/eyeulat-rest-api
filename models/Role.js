import App from '../config/model';

class Role extends App.Model {
    get tableName() { return 'r_roles'; }

    get hidden() {
        return [];
    }
}

export default App.model('Role', Role);