// Initialize required dependencies
import 'dotenv/config';
import express from 'express';
import bodyParser from 'body-parser';

// Include all routes
import { v1 as apiv1 } from './routes/api';

// Set up the express app
const app = express();

// Parse incoming requests data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Let Express use the routes
app.use('/api/v1', apiv1);

const PORT = 5000;

app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`)
});
